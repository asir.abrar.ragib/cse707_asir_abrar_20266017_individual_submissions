Optional Individual Submission: Extra-Credit/Bonus

Student Name: Asir Abrar
Student ID: 20266017

Course Name: **Google Cloud Platform Fundamentals: Core Infrastructure**

Credential URL: https://www.coursera.org/account/accomplishments/certificate/VE4QJ2ELK7SQ

Summary: This is a very basic course on Google Cloud. It provides the concepts and the technology, that needs to work in GCP.  This course consists of computing and storage services available in Google Cloud Platform, including Google App Engine, Google Compute Engine, Google Kubernetes Engine, Google Cloud Storage, Google Cloud SQL, and BigQuery. A very good thing about this course is, it provides a hands-on lab experience with very helpful documents.

Issuing Organization: Coursera
Issue Date: November 2020
Expiration Date: This certification does not expire
Credential ID: VE4QJ2ELK7SQ


